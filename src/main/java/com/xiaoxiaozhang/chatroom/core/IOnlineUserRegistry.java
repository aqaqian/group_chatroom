package com.xiaoxiaozhang.chatroom.core;

import com.xiaoxiaozhang.chatroom.model.OnlineUser;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 张晓倩
 * Date: 2022-03-01
 * Time: 10:24
 */
public interface IOnlineUserRegistry {
    void online(OnlineUser user) throws IOException;
    void offline(OnlineUser user);
}
