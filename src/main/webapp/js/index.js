// 登录后才能使用
//判断用户是否完成了登录
function checkLogin() {
    var xhr = new XMLHttpRequest();
    xhr.open("GET", "/group_chatroom/api/get-current-user.json");
    xhr.onload = function () {
        var r = JSON.parse(xhr.responseText);
        if(r.currentUser) {
            main(r.currentUser);
        } else {
            alert("请登录");
            window.location = "/group_chatroom/login.html";
        }
    }
    xhr.send();
}

checkLogin();
function main() {
//登陆之后才会执行的操作
    var ol = document.querySelector("#聊天内容区");
    var textarea = document.querySelector("#输入框");
    var sendBtn = document.querySelector("#发送按钮");

    var ws = new WebSocket("ws://127.0.0.1:8080/group_chatroom/message");
    ws.onmessage = function (e) {
        var message = JSON.parse(e.data);
        var liHTML = `<li>
            <div class="聊天头像"><img src="/group_chatroom/img/flower.jpg"></div>
            <div class="聊天内容部分">
                <div class="聊天昵称">${message.nickname}</div>
                <div class="聊天内容">${message.content}</div>
            </div>
        </li>`;
        ol.innerHTML += liHTML;
    }

    sendBtn.onclick = function () {
        var content = textarea.value;
        textarea.value = '';
        ws.send(content);
    }
    textarea.onkeydown = function (e) {
        if (e.keyCode == 13) {
            var content = textarea.value;
            textarea.value = '';
            ws.send(content);
        }
    }

    var 群成员总计 = document.querySelector("#群成员总计")
    var 群成员名单 = document.querySelector("#群成员名单 ol")
    function updateUserList() {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/group_chatroom/api/user-list.json");
        xhr.onload = function() {
            var result = JSON.parse(xhr.responseText);
            群成员总计.innerText = `群成员 ${result.onlineCount} / ${result.totalCount}`;
            群成员名单.innerHTML =  '';
            for (var i in result.userList) {
                var user = result.userList[i];
                var html;
                if (user.online) {
                    html =`<li>
                               <div class="群成员头像">
                                   <img src="https://up.bizhizu.com/pic_source/44/bd/93/44bd93671fe896aa17cb1349e7f81e3c.jpg">
                               </div>
                               <div class="群成员昵称">${user.nickname}</div>
                           </li>`
                } else {
                    html = `<li class="不在线">
                                <div class="群成员头像">
                                    <img src="/group_chatroom/img/flower.jpg">
                                </div>
                                <div class="群成员昵称">${user.nickname}</div>
                            </li>`
                }
                群成员名单.innerHTML += html;
            }
        }
        xhr.send();
    }
    updateUserList();
    setInterval(updateUserList, 1000);
}