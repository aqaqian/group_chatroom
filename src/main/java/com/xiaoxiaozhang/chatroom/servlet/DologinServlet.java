package com.xiaoxiaozhang.chatroom.servlet;

import com.xiaoxiaozhang.chatroom.model.User;
import com.xiaoxiaozhang.chatroom.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 张晓倩
 * Date: 2022-03-01
 * Time: 14:21
 */
@WebServlet("/do-login")
public class DologinServlet extends HttpServlet {
    public DologinServlet() {
        System.out.println("DologinServlet()");
    }

    private final UserService userService = UserService.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        
        User user = userService.login(username, password);
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/plain");
        PrintWriter writer = resp.getWriter();
        if (user != null) {
           HttpSession session = req.getSession();
           session.setAttribute("currentUser", user);
            writer.println("登陆成功");
            resp.sendRedirect("/group_chatroom/");
        } else {
            writer.println("登录失败");
        }
    }
}
