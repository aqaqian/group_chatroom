package com.xiaoxiaozhang.chatroom.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 张晓倩
 * Date: 2022-03-02
 * Time: 0:17
 */
public class UserListResult {
    public Integer onlineCount;
    public Integer totalCount;
    public List<UserListUser> userList = new ArrayList<>();
}
