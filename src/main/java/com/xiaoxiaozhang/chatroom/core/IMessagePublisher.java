package com.xiaoxiaozhang.chatroom.core;

import com.xiaoxiaozhang.chatroom.model.User;

import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 张晓倩
 * Date: 2022-03-01
 * Time: 10:26
 */
public interface IMessagePublisher {
    void publish(User user, String messageText) throws IOException;
}
