CREATE SCHEMA `group_chatroom` DEFAULT CHARACTER SET utf8mb4 ;

CREATE TABLE `group_chatroom`.`users` (
  `uid` INT NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(60) NOT NULL,
  `nickname` VARCHAR(60) NOT NULL,
  `password` CHAR(60) NOT NULL,
  `logout_at` DATETIME NULL,
  PRIMARY KEY (`uid`),
  UNIQUE INDEX `username_UNIQUE` (`username` ASC));

  CREATE TABLE `group_chatroom`.`messages` (
  `mid` INT NOT NULL AUTO_INCREMENT,
  `uid` INT NOT NULL,
  `content` TEXT NOT NULL,
  `published_at` DATETIME NOT NULL,
  PRIMARY KEY (`mid`));