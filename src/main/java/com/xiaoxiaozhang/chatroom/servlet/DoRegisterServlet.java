package com.xiaoxiaozhang.chatroom.servlet;

import com.xiaoxiaozhang.chatroom.model.User;
import com.xiaoxiaozhang.chatroom.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created with IntelliJ IDEA.
 * Description:
 * User: 张晓倩
 * Date: 2022-03-01
 * Time: 14:22
 */
@WebServlet("/do-register")
public class DoRegisterServlet extends HttpServlet {
    public DoRegisterServlet() {
        System.out.println("DoRegisterServlet()");
    }
    private final UserService userService = UserService.getInstance();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        String username = req.getParameter("username");
        String nickname = req.getParameter("nickname");
        String password = req.getParameter("password");

        User user = userService.register(username, nickname, password);
        resp.setContentType("text/plain");
        resp.setCharacterEncoding("utf-8");
        req.getSession().setAttribute("currentUser", user);
        resp.getWriter().println("注册并登录成功");
        resp.sendRedirect("/group_chatroom/");
    }
}
